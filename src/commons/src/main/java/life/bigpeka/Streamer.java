package life.bigpeka;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Streamer {
  private final long id;

  private final String name;

  public Streamer(@JsonProperty("id") long id, @JsonProperty("name") String name) {
    this.id = id;
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return "FunstreamTvStreamer{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}
