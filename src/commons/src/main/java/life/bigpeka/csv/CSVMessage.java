package life.bigpeka.csv;

import com.google.common.base.*;
import com.google.common.collect.Lists;
import life.bigpeka.User;
import java.io.Serializable;
import java.time.Instant;
import java.util.*;
import java.util.Optional;
import java.util.stream.*;

public class CSVMessage implements Serializable {
  private final long id;

  private final User from;

  private final User to;

  private final List<String> stems;

  private final Instant time;

  public CSVMessage(final long id, final User from,
      final User to, final List<String> stems, final Instant time) {
    this.id = id;
    this.from = from;
    this.to = to;
    this.stems = stems;
    this.time = time;
  }

  public long getId() {
    return id;
  }

  public User getFrom() {
    return from;
  }

  public User getTo() {
    return to;
  }

  public List<String> getStems() {
    return stems;
  }

  public Instant getTime() {
    return time;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }

    final CSVMessage that = (CSVMessage) o;

    if (id != that.id) { return false; }
    if (!from.equals(that.from)) { return false; }
    if (!to.equals(that.to)) { return false; }
    if (!stems.equals(that.stems)) { return false; }
    return time.equals(that.time);
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + from.hashCode();
    result = 31 * result + to.hashCode();
    result = 31 * result + stems.hashCode();
    result = 31 * result + time.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "CSVMessage{" +
        "id=" + id +
        ", from=" + from +
        ", to=" + to +
        ", stems=" + stems +
        ", time=" + time +
        '}';
  }

  public static Optional<CSVMessage> readFromLine(final String line) {
    try {
      final ArrayList<String> csvColumns = StreamSupport
          .stream(Splitter.on(",").split(line).spliterator(), false)
          .map(s -> s.replace("\"", ""))
          .collect(Collectors.toCollection(ArrayList::new));
      Preconditions.checkState(csvColumns.size() == 7, "Wrong number of columns in input line",
          csvColumns);

      final int id = Integer.parseInt(csvColumns.get(0));
      final int from = Integer.parseInt(csvColumns.get(1));
      final String fromName = csvColumns.get(2);
      final int to = Integer.parseInt(csvColumns.get(3));
      final String toName = csvColumns.get(4);
      final ArrayList<String> stems =
          Lists.newArrayList(
              Splitter.on("|")
                  .omitEmptyStrings()
                  .trimResults()
                  .split(csvColumns.get(5)));
      final Instant time = Instant.ofEpochSecond(Long.parseLong(csvColumns.get(6)));
      final User userFrom = new User(fromName, from);
      final User userTo = new User(toName, to);

      return Optional.of(new CSVMessage(id, userFrom, userTo, stems, time));
    } catch (Exception anyException) {
      return Optional.empty();
    }
  }
}
