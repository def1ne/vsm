package life.bigpeka.csv;

import com.google.common.base.Preconditions;
import org.slf4j.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.*;

public class CSVReader {
  private static final Logger LOGGER = LoggerFactory.getLogger(CSVReader.class);

  public CSVReader() {
  }

  public List<CSVMessage> readFile(final File csvFile) {
    Preconditions.checkArgument(!csvFile.isDirectory());
    try {
      return Files.lines(csvFile.toPath(), Charset.forName("UTF-8"))
          .map(CSVMessage::readFromLine)
          .filter(Optional::isPresent)
          .map(Optional::get)
          .collect(Collectors.toCollection(ArrayList::new));
    } catch (IOException e) {
      throw new IllegalStateException("Unsupported file format");
    }
  }

  public List<CSVMessage> readDirectory(final File csvDirectory) {
    Preconditions.checkArgument(csvDirectory.isDirectory());
    return Arrays.stream(csvDirectory.listFiles((dir, name) -> name.endsWith("csv")))
        .flatMap(file -> readFile(file).stream())
        .collect(Collectors.toCollection(ArrayList::new));
  }
}
