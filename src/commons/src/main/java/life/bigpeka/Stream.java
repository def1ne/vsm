package life.bigpeka;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Stream {
    /* not complete implementation, example of full object:
        {
            adult: true
            category: {id: 26, slug: "letsplay-rpg", name: "RPG"}
            description: "Полное прохождение заказал : asdfgke."
            id: 157064
            image: "/images/streams/rcojgzs8n94.jpg"
            name: "UnEPIC "
            rating: 1157
            start_at: 0
            streamer: {id: 16869, name: "Dave Jones"}
            thumbnail: "/images/streams/thumb-rcojgzs8n94.jpg"
        }
     */

  private final Streamer streamer;

  public Stream(@JsonProperty("streamer") Streamer streamer) {
    this.streamer = streamer;
  }

  public Streamer getStreamer() {
    return streamer;
  }

  @Override
  public String toString() {
    return "Stream{" +
        "streamer=" + streamer +
        '}';
  }
}
