package life.bigpeka;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class StreamsResponse {
  /* not complete implementation, should also have category field containing parent and current
  categories */

  private final List<Stream> streams;

  public StreamsResponse(@JsonProperty("content") List<Stream> streams) {
    this.streams = streams;
  }

  public List<Stream> getStreams() {
    return streams;
  }

  @Override
  public String toString() {
    return "StreamsResponseEntity{" +
        "streams=" + streams +
        '}';
  }
}
