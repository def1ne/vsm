package life.bigpeka;

import java.time.*;
import java.util.Objects;

public class Message {
  private final long id;

  private final String channel;

  private final User from;

  private final User to;

  private final String text;

  private final Instant time;

  public Message(final long id, final String channel, final User from,
      final User to, final String text, final Instant time) {
    this.id = id;
    this.channel = channel;
    this.from = from;
    this.to = to;
    this.text = text;
    this.time = time;
  }

  public long getId() {
    return id;
  }

  public String getChannel() {
    return channel;
  }

  public User getFrom() {
    return from;
  }

  public User getTo() {
    return to;
  }

  public String getText() {
    return text;
  }

  public Instant getTime() {
    return time;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, channel, from, to, text, time);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }
    final Message message = (Message) o;
    return Objects.equals(id, message.id) &&
        Objects.equals(channel, message.channel) &&
        Objects.equals(from, message.from) &&
        Objects.equals(to, message.to) &&
        Objects.equals(text, message.text) &&
        Objects.equals(time, message.time);
  }

  @Override
  public String toString() {
    return "FunstreamTvMessage{" +
        "id=" + id +
        ", channel='" + channel + '\'' +
        ", from=" + from +
        ", to=" + to +
        ", text='" + text + '\'' +
        ", time=" + time +
        '}';
  }
}
