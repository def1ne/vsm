package life.bigpeka.csv;

import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class CSVMessageTest {

  @Test
  public void shouldParseLineFromStandardCSVFile()
  throws Exception {
    final String theLine
        = "\"1684052\",\"52100\",\"akandr\",\"49437\",\"NightGhost\",\"уныл|член|стояк|порвет|"
        + "штанин|черт|peka\",\"1434496035\"";

    final CSVMessage csvMessage = CSVMessage.readFromLine(theLine).get();
    assertThat(csvMessage).isNotNull();
    assertThat(csvMessage.getFrom().getId()).isEqualTo(52100);
    assertThat(csvMessage.getTo().getId()).isEqualTo(49437);
  }
}