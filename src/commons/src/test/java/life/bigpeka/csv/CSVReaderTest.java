package life.bigpeka.csv;

import com.google.common.io.Resources;
import org.junit.Test;
import java.io.File;
import java.net.*;
import java.util.List;
import static org.assertj.core.api.Assertions.*;

public class CSVReaderTest {
  @Test
  public void shouldReadStandardCSVFile()
  throws URISyntaxException {
    // small messages file exists in repo for this test
    final File messages = new File(Resources.getResource("messages.csv").toURI());
    final List<CSVMessage> csvMessages = new CSVReader().readFile(messages);

    assertThat(csvMessages).isNotNull();
    assertThat(csvMessages.size()).isEqualTo(15);
  }

  @Test
  public void shouldReadStandardCSVDirectory()
  throws URISyntaxException {
    // small messages file exists in repo for this test
    final File messages = new File(Resources.getResource("messages.csv").toURI());
    final List<CSVMessage> csvMessages = new CSVReader().readDirectory(
        messages.getParentFile());

    assertThat(csvMessages).isNotNull();
    assertThat(csvMessages.size()).isEqualTo(15);
  }
}