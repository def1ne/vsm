package life.bigpeka.web.services.user.statistics;

import com.google.common.io.Resources;
import io.dropwizard.testing.junit.DropwizardAppRule;
import life.bigpeka.*;
import life.bigpeka.web.api.*;
import org.junit.*;
import static org.assertj.core.api.Assertions.*;

public
class UserStatisticsServiceTest {
  @ClassRule
  public static final DropwizardAppRule<BPLConfiguration> RULE =
      new DropwizardAppRule<BPLConfiguration>(BPLApplication.class,
          Resources.getResource("bpl-test-config.yml").getFile());

  @Test
  public
  void shouldReturn1487ForValues() {
    final UserStatisticsApi userStatisticsApi = UserStatisticsClients.bindTo(
        "localhost",
        RULE.getTestSupport().getLocalPort());
    final String theName = "vasya";
    final UserStatistics vasya = userStatisticsApi.getForUserId(theName);
    assertThat(vasya).isNotNull();
    assertThat(vasya.getName()).isEqualTo(theName);
    assertThat(vasya.getPekaCount()).isEqualTo(1487);
  }
}