package life.bigpeka.web;

import com.google.common.io.Resources;
import io.dropwizard.testing.junit.DropwizardAppRule;
import life.bigpeka.*;
import org.glassfish.jersey.client.*;
import org.junit.*;
import javax.ws.rs.client.Client;
import static org.assertj.core.api.Assertions.assertThat;

public class HtmlBundleTest {
  @ClassRule
  public static final DropwizardAppRule<BPLConfiguration> RULE =
      new DropwizardAppRule<BPLConfiguration>(BPLApplication.class,
          Resources.getResource("bpl-test-config.yml").getFile());

  @Test
  public void indexShouldBeServedWithTestConfig() {
    Client client = new JerseyClientBuilder()
        .build();
    final String s = client.target("http://localhost:" + RULE.getTestSupport().getLocalPort())
        .request()
        .get(String.class);
    assertThat(s).startsWith("<!DOCTYPE html>");
  }
}