package life.bigpeka.vsm;

import life.bigpeka.csv.CSVMessage;
import life.bigpeka.vsm.tfidf.*;
import org.jblas.DoubleMatrix;
import org.junit.Test;
import java.util.*;
import static org.assertj.core.api.Assertions.assertThat;


public class TfIdfTest {
  @Test
  public void tfidfTest() {
    List<CSVMessage> csvMessages = new ArrayList<>();
    ArrayList<String> stemsMessage1 = new ArrayList<>();
    stemsMessage1.add("пека");
    stemsMessage1.add("пека");
    stemsMessage1.add("пека");
    CSVMessage message1 = new CSVMessage(0, null, null, stemsMessage1, null);
    ArrayList<String> stemsMessage2 = new ArrayList<>();
    stemsMessage2.add("пека");
    stemsMessage2.add("адольф");
    CSVMessage message2 = new CSVMessage(0, null, null, stemsMessage2, null);
    csvMessages.add(message1);
    csvMessages.add(message2);

    HashSet<String> stems = new HashSet<>();
    stems.add("пека");
    stems.add("адольф");
    /**
     * У нас получается tf  - |    1        0.5   |
     *                        |    0        0.5   |
     *
     *                 idf  - |  log(1)      0    |
     *                        |    0       log(2) |
     *
     *               tf*idf - |    0         0    |
     *                        |    0     log(2)/2 |
     * */
    TfIdfRanker ranker = new TfIdfRanker(new SequentCounter());
    DoubleMatrix vsm = ranker.process(csvMessages, stems);
    assertThat(vsm.data).containsExactly(0, 0, 0, Math.log(2)/2);
  }
}
