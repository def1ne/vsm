package life.bigpeka.retrieval;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import life.bigpeka.Message;
import java.util.concurrent.*;
import static org.assertj.core.api.Assertions.assertThat;

public class FunstreamTvClientTest {
  @Test
  public void shouldConnectAndSuccessfullyParseMessages()
  throws ExecutionException, InterruptedException {
    CompletableFuture<Message> awaitedMessage = new CompletableFuture<>();

    final FunstreamTvClient funstreamTvClient =
        new FunstreamTvClient(
            message -> awaitedMessage.complete(message),
            ImmutableList.of("main"));
    funstreamTvClient.connect();

    final Message message = awaitedMessage.get();
    assertThat(message.getId()).isNotNull();
    assertThat(message.getChannel()).isNotNull();
    assertThat(message.getFrom()).isNotNull();
    assertThat(message.getTo()).isNotNull();
    assertThat(message.getText()).isNotNull();
    assertThat(message.getTime()).isNotNull();

    assertThat(message.getText().length()).isGreaterThan(0);
    assertThat(message.getFrom().getId()).isGreaterThan(0);
    assertThat(message.getFrom().getName().length()).isGreaterThan(0);
  }
}