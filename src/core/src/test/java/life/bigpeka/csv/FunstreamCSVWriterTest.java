package life.bigpeka.csv;

import life.bigpeka.*;
import org.junit.Test;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import static org.assertj.core.api.Assertions.*;

public class FunstreamCSVWriterTest {
  @Test
  public void shouldGenerateParseableCSVArray() {
    final Message aMessage = new Message(
        0L,
        "test",
        new User("name1", 123L),
        new User("name2", 456L),
        "qqqasdf",
        Instant.now());

    final String[] arrayForCSV = FunstreamCSVWriter.csvArrayFromMessage(aMessage, "stem|otherStem");

    final String lineFromArray = Arrays.stream(arrayForCSV)
        .map((string) -> "\"" + string + "\"")
        .collect(Collectors.joining(","));

    final CSVMessage csvMessage = CSVMessage.readFromLine(lineFromArray).get();
    assertThat(csvMessage).isNotNull();
    assertThat(csvMessage.getFrom().getId()).isEqualTo(123L);
    assertThat(csvMessage.getTo().getId()).isEqualTo(456L);
  }
}