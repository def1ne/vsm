package life.bigpeka;

import com.google.common.collect.Lists;
import wordcloud.*;
import wordcloud.bg.RectangleBackground;
import wordcloud.font.*;
import wordcloud.font.scale.LinearFontScalar;
import java.util.ArrayList;

public class KumoDemonstration {
  public static void main(String[] args) {
    final ArrayList<WordFrequency> wordFrequencies = Lists.newArrayList(
        new WordFrequency("pussy", 14),
        new WordFrequency("dick", 18));
    final WordCloud wordCloud = new WordCloud(600, 600, CollisionMode.RECTANGLE);
    wordCloud.setPadding(0);
    wordCloud.setBackground(new RectangleBackground(600, 600));
    wordCloud.setFontScalar(new LinearFontScalar(10, 40));
    wordCloud.setCloudFont(new CloudFont("Arial", FontWeight.PLAIN));
    wordCloud.build(wordFrequencies);

    wordCloud.writeToFile("test.png");
  }
}
