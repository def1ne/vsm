package life.bigpeka;

import io.dropwizard.Configuration;

public class BPLConfiguration extends Configuration {
  private String testField;

  public String getTestField() {
    return testField;
  }

  public void setTestField(String name) {
    this.testField = name;
  }
}