package life.bigpeka.processing.nlp.social.graph.edges;


import life.bigpeka.processing.nlp.social.graph.vertices.Channel;
import life.bigpeka.processing.nlp.social.graph.vertices.User;

public class EdgeFactory {
    /**Создает связь
     *
     * @param targetVertex Класс вершины, на которую направлена связь
     * @return связь нужного класса
     */
    public static Edge getEdge(Class targetVertex) {

        if (targetVertex == Channel.class) {
            return new ChannelEdge();
        }
        if (targetVertex == User.class) {
            return new UserEdge();
        }
        return null;
    }
}
