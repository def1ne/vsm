package life.bigpeka.processing.nlp;

import java.util.List;
import java.util.stream.Collectors;

class PostProcessor {
  public static List<String> postProcess(final List<String> stemmedWords) {
    return stemmedWords.stream()
        .filter((t) -> t.length() > 2)
        .collect(Collectors.toList());
  }
}