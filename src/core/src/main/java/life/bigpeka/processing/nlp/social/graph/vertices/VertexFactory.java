package life.bigpeka.processing.nlp.social.graph.vertices;

public class VertexFactory {
    /**Создает вершину, принимает имя и класс вершины.
     *
     * @param name Имя вершины
     * @param vertexClass Класс вершины
     * @return Вершина нужного класса
     */
    public static Vertex getVertex(String name, Class vertexClass) {

        if (vertexClass == Channel.class) {
            return new Channel(name);
        }
        if (vertexClass == User.class) {
            return new User(name);
        }
        return null;
    }
}
