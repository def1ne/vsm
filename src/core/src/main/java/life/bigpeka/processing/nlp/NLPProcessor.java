package life.bigpeka.processing.nlp;

import java.util.ArrayList;
import java.util.List;

public class NLPProcessor {
  public static List<String> process(final String srcString) {
    final ArrayList<String> preProcessedMessage = PreProcessor.preProcess(srcString);
    final ArrayList<String> cleanedMessage = StopWordsCleaner.clean(preProcessedMessage);
    final ArrayList<String> stemmedWords = Stemmer.stem(cleanedMessage);
    final List<String> strings = PostProcessor.postProcess(stemmedWords);
    return strings;
  }
}