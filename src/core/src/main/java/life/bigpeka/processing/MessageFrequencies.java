package life.bigpeka.processing;

import com.google.common.collect.Maps;
import com.google.common.io.Resources;
import life.bigpeka.csv.*;
import org.tc33.jheatchart.HeatChart;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.time.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class MessageFrequencies {
  public static void main(String[] args)
  throws IOException, URISyntaxException {
    final URL resource = Resources.getResource("messages_18.06.csv");
    final URI uri = resource.toURI();
    CSVReader reader = new CSVReader();
    List<CSVMessage> messages = reader.readFile(new File(uri));

    Map<String, Map<Integer, Long>> counts = messages
        .stream()
        .map(msg -> new NameDatePair(msg.getFrom().getName(),
            LocalDateTime.ofInstant(msg.getTime(), ZoneId.systemDefault()).getHour()))
        .collect(
            Collectors.groupingBy(
                NameDatePair::getName,
                Collectors.groupingBy(NameDatePair::getHour, Collectors.counting())));

    final double globalAverage = counts.entrySet()
        .stream()
        .filter(e -> e.getValue().size() > 5)
        .map((e) -> e.getValue().values().stream().collect(Collectors.averagingLong(l -> l)))
        .collect(Collectors.averagingDouble(value -> value));

    // only more than global average
    counts = counts.entrySet().stream()
        .filter(e -> e.getValue().size() > 5)
        .filter(e -> e.getValue().values().stream()
            .mapToLong(value -> value).average().getAsDouble() > globalAverage)
        .collect(() -> Maps.newTreeMap(),
            (l, r) -> l.put(r.getKey(), r.getValue()),
            (acc, e) -> acc.putAll(e));

    final Map<String, Map<Integer, Long>> extractedCounts = counts.entrySet().stream()
        .sorted((o1, o2) -> (computeStatistic(o2) > computeStatistic(o1)) ? 1 : -1)
        .limit(60)
        .collect(() -> Maps.newTreeMap(),
            (l, r) -> l.put(r.getKey(), r.getValue()),
            (acc, e) -> acc.putAll(e));

    //transform data to use with HeatChart library
    int userCount = extractedCounts.size();

    int hourOffset = 9;
    double[][] data = new double[24 - hourOffset][];
    String[] xLabels = new String[userCount];
    String[] yLabels = new String[24 - hourOffset];
    for (int i = 0; i < 24 - hourOffset; ++i) {
      data[i] = new double[userCount];
      yLabels[i] = String.valueOf(i + hourOffset);
    }
    int i = 0;
    for (Map.Entry<String, Map<Integer, Long>> entry : extractedCounts.entrySet()) {
      for (int j = 0; j < 24 - hourOffset; j++) {
        data[j][i] = entry.getValue().getOrDefault(j + hourOffset, 0l);
      }
      xLabels[i] = entry.getKey();
      ++i;
    }

    HeatChart heatChart = new HeatChart(data);
    heatChart.setHighValueColour(Color.RED);
    heatChart.setXValues(xLabels);
    heatChart.setYValues(yLabels);
    heatChart.saveToFile(new File("heatmap.png"));
  }

  private static double computeStatistic(final Map.Entry<String, Map<Integer, Long>> userEntry) {
    // нужно получить число, которое характеризует разброс
    // сумма квадратов разностей от среднего
    final double average = userEntry.getValue().values()
        .stream().mapToLong(value -> value).average().getAsDouble();
    return userEntry.getValue().values().stream()
        .map(aLong -> Math.pow(aLong.doubleValue() - average, 2))
        .mapToDouble(value -> value)
        .sum();
  }

  private static class NameDatePair {
    private String name;

    private int hour;

    public NameDatePair(final String name, final int hour) {
      this.name = name;
      this.hour = hour;
    }

    public String getName() {
      return name;
    }

    public int getHour() {
      return hour;
    }
  }
}
