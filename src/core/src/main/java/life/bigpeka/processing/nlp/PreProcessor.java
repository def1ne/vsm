package life.bigpeka.processing.nlp;

import com.google.common.collect.Lists;

import java.util.*;
import java.util.regex.Pattern;

class PreProcessor {

  private static Pattern pattern = Pattern.compile("[^a-z^а-я]+");

  public static ArrayList<String> preProcess(String srcMessage) {

      srcMessage = srcMessage.toLowerCase();
      srcMessage = srcMessage.replace("й", "и");
      srcMessage = srcMessage.replace("ё", "е");

      ArrayList<String> strings = Lists.newArrayList(Arrays.asList(pattern.split(srcMessage)));

    return strings;
  }
}