package life.bigpeka.vsm.tfidf;

import org.jblas.DoubleMatrix;
import org.slf4j.*;
import life.bigpeka.csv.CSVMessage;
import java.util.*;

/**
 * Базовый вычислитель tf и idf
 */
public abstract class Counter {
  public abstract void process(final List<CSVMessage> messages, final Set<String> stems,
      final DoubleMatrix tfWrapper, final DoubleMatrix idfWrapper);

  /**
   * Вычисляет tf и idf для одного стема, пишит их в матрицы tfWrapper и idfWrapper по индексу
   * stemIndex
   * @param messages сообщения
   * @param stem стем
   * @param stemIndex индекс стема(нужен чтобы знать, куда писать в tfWrapper и idfWrapper)
   * @param tfWrapper хранилище результатов вычисления tf
   * @param idfWrapper хранилище результатов вычисления idf
   */
  protected static void processStem(final List<CSVMessage> messages, final String stem, final int
      stemIndex, final DoubleMatrix tfWrapper, final DoubleMatrix idfWrapper) {
    if(LOGGER.isInfoEnabled())
      LOGGER.info("start process stem "+stem+", stem index "+stemIndex);
    double stemOccurrence = 0;
    int messageIndex = 0;
    for(CSVMessage message: messages) {
      double stemMessageOccurrence = 0;
      List<String> messageStems = message.getStems();
      for(String messageStem: messageStems) {
        if(stem.equals(messageStem))
          stemMessageOccurrence++;
      }
      if(stemMessageOccurrence > 0) {
        stemOccurrence++;
        tfWrapper.put(stemIndex, messageIndex, stemMessageOccurrence/messageStems.size());
      }
      messageIndex++;
    }
    idfWrapper.put(stemIndex, stemIndex, Math.log(messages.size()/stemOccurrence));
    if(LOGGER.isInfoEnabled())
      LOGGER.info("end process stem "+stem+", stem index "+stemIndex);
  }

  private static final Logger LOGGER = LoggerFactory.getLogger(Counter.class);
}
