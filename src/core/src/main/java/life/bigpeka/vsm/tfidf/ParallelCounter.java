package life.bigpeka.vsm.tfidf;

import org.jblas.DoubleMatrix;
import org.slf4j.*;
import life.bigpeka.csv.CSVMessage;
import java.util.*;
import java.util.concurrent.*;

/**
 * Параллельно вычисляет матрицы tf и idf
 * @todo требуется, чтобы с этими матрицами во время вычисления работали только ParallelCounterTask
 * @todo в самих ParallelCounterTask подразумивается, что никакие из них не работают с одним stem-ом
 * @todo в самом ParallelCounterTask-е тоже можно распаралерить подсчёт, но у меня быстрее не станет
 */
public class ParallelCounter extends Counter {
  public ParallelCounter() {
    this(Runtime.getRuntime().availableProcessors()/2);
  }

  public ParallelCounter(int parallelismLevel) {
    int currentParallelismLevel = 1;
    int parallelismLevelMax = Runtime.getRuntime().availableProcessors();
    if(parallelismLevel > parallelismLevelMax)
      currentParallelismLevel = parallelismLevelMax;
    else if(parallelismLevel > 1)
      currentParallelismLevel = parallelismLevel;
    executorService = Executors.newFixedThreadPool(currentParallelismLevel);
  }

  @Override
  public void process(final List<CSVMessage> messages, final Set<String> stems,
      final DoubleMatrix tfWrapper, final DoubleMatrix idfWrapper) {
    int stemIndex = 0;
    for(String stem: stems) {
      ParallelCounterTask task = new ParallelCounterTask(messages, stem, stemIndex, tfWrapper,
          idfWrapper);
      executorService.submit(task);
      stemIndex++;
    }
    try {
      executorService.shutdown();
      while(true) { //@todo стоит как-то иначе ждать завершения
        if(executorService.awaitTermination(1000, TimeUnit.SECONDS))
          break;
      }
    }
    catch(InterruptedException ex) {
      LOGGER.error("can't wait executor service termination", ex);
    }
  }

  protected final ExecutorService executorService;
  private static final Logger LOGGER = LoggerFactory.getLogger(ParallelCounter.class);

  /**
   * Задача вычисления tf и idf для одного стема
   */
  protected static class ParallelCounterTask implements Runnable {
    public ParallelCounterTask(List<CSVMessage> messages, String stem, int stemIndex,
        DoubleMatrix tfWrapper, DoubleMatrix idfWrapper) {
      this.messages = messages;
      this.stem = stem;
      this.stemIndex = stemIndex;
      this.tfWrapper = tfWrapper;
      this.idfWrapper = idfWrapper;
    }

    @Override
    public void run() {
      processStem(messages, stem, stemIndex, tfWrapper, idfWrapper);
    }

    protected final List<CSVMessage> messages;
    protected final String stem;
    protected final int stemIndex;
    protected final DoubleMatrix tfWrapper;
    protected final DoubleMatrix idfWrapper;
  }
}
