package life.bigpeka.vsm.tfidf;

import org.jblas.DoubleMatrix;
import life.bigpeka.csv.CSVMessage;
import java.util.*;

/**
 * Вычисляет матрицы tf и idf последовательно
 */
public class SequentCounter extends Counter {
  @Override
  public void process(final List<CSVMessage> messages, final Set<String> stems,
      final DoubleMatrix tfWrapper, final DoubleMatrix idfWrapper) {
    int stemIndex = 0;
    for(String stem: stems) {
      processStem(messages, stem, stemIndex, tfWrapper, idfWrapper);
      stemIndex++;
    }
  }
}
