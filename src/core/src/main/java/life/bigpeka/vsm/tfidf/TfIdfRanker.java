package life.bigpeka.vsm.tfidf;

import org.jblas.DoubleMatrix;
import life.bigpeka.csv.CSVMessage;
import java.util.*;

/**
 * Рассчитывает наши tf-idf веса
 */
public class TfIdfRanker {
  public TfIdfRanker(Counter counter) {
    this.counter = counter;
  }

  public DoubleMatrix process(final List<CSVMessage> messages, final Set<String> stems)
  {
    DoubleMatrix tf = DoubleMatrix.zeros(stems.size(), messages.size());
    DoubleMatrix idf = DoubleMatrix.zeros(stems.size(), stems.size());
    counter.process(messages, stems, tf, idf);
    //Умножаем (s*s)*(s*m), где idf - диагональная матрица с коэфицентами idf размера s*s
    return idf.mmul(tf);
  }

  protected final Counter counter;
}
