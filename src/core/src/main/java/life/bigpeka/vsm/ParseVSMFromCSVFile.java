package life.bigpeka.vsm;

import life.bigpeka.csv.*;
import life.bigpeka.vsm.tfidf.*;
import org.jblas.DoubleMatrix;
import java.io.File;
import java.util.*;

public class ParseVSMFromCSVFile {
  public static void main(String[] args) {
    //@todo тестировать пока что тут
    //"C://main/work/Java/big-peka-life/src/main/resources/messages_20hrs_first_dataset.csv"
    if(args.length < 1)
      return;
    File csvFile = new File(args[0]);
    if(!csvFile.exists())
      return;

    CSVReader csvReader = new CSVReader();
    List<CSVMessage> csvMessages = csvReader.readFile(csvFile);

    HashSet<String> stems = new HashSet<>();
    int stemsMaxSize = 1000;
    for(CSVMessage message: csvMessages) {
      for(String messageStem: message.getStems()) {
        if(stems.add(messageStem)) {
          if(stemsMaxSize <= 1)
            break;
          stemsMaxSize--;
        }
      }
      if(stemsMaxSize <= 1)
        break;
    }

    TfIdfRanker ranker = new TfIdfRanker(new ParallelCounter(5));
//    TfIdfRanker ranker = new TfIdfRanker(new SequentCounter());
    long start = System.currentTimeMillis();
    DoubleMatrix vsm = ranker.process(csvMessages, stems);
    long end = System.currentTimeMillis();
    System.out.println("start=" + start + ", end=" + end + ", total=" + (end - start));
  }
}
