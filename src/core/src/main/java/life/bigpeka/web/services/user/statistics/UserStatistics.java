package life.bigpeka.web.services.user.statistics;

public class UserStatistics {
  private String name;

  private Integer pekaCount;

  public UserStatistics(final String name, final Integer pekaCount) {
    this.name = name;
    this.pekaCount = pekaCount;
  }

  public UserStatistics() {
  }

  public String getName() {
    return name;
  }

  public Integer getPekaCount() {
    return pekaCount;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (pekaCount != null ? pekaCount.hashCode() : 0);
    return result;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }

    final UserStatistics that = (UserStatistics) o;

    if (name != null ? !name.equals(that.name) : that.name != null) { return false; }
    return !(pekaCount != null ? !pekaCount.equals(that.pekaCount) : that.pekaCount != null);
  }

  @Override
  public String toString() {
    return "UserStatistics{" +
        "name='" + name + '\'' +
        ", pekaCount=" + pekaCount +
        '}';
  }
}
