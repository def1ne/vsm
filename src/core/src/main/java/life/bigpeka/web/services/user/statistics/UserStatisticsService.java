package life.bigpeka.web.services.user.statistics;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/v1/userStatistics")
public class UserStatisticsService {
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("byName")
  public UserStatistics getForUserId(@QueryParam("userName") String userName) {
    return new UserStatistics(userName, 1487);
  }
}
