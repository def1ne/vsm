package life.bigpeka.csv;

import com.opencsv.CSVWriter;
import life.bigpeka.Message;
import life.bigpeka.processing.nlp.NLPProcessor;
import life.bigpeka.retrieval.*;
import org.slf4j.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import static java.util.stream.Collectors.joining;

public class FunstreamCSVWriter {
  public static void main(String[] args)
  throws IOException {
    Logger logger = LoggerFactory.getLogger(FunstreamCSVWriter.class);
    CSVWriter writer = new CSVWriter(new FileWriter("messages.csv", false), ',');

    FunstreamTvClient client = new FunstreamTvClient(new FunstreamTvEventListener() {
      @Override
      public void onConnect() {

      }

      @Override
      public void onDisconnect() {
        try {
          writer.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }

      @Override
      public void onMessage(final Message message) {
        final List<String> process = NLPProcessor.process(message.getText());
        final String reducedStems = process.stream().collect(joining("|"));
        writer.writeNext(csvArrayFromMessage(message, reducedStems));
      }

      @Override
      public void onJoin(final String channel) {

      }
    }, Collections.singletonList("main"));

    client.connect();

    new FunstreamTvStreamTracker()
        .onStreamOnline(s -> {
          logger.debug("joining {}", s);
          client.join(s);
        })
        .onStreamOffline(s -> {
          logger.debug("leaving {}", s);
          client.leave(s);
        }).start(1, TimeUnit.MINUTES);
  }

  public static String[] csvArrayFromMessage(final Message message,
      final String reducedStems) {
    return new String[]{
        "" + message.getId(),
        "" + message.getFrom().getId(),
        message.getFrom().getName(),
        "" + message.getTo().getId(),
        message.getTo().getName(),
        reducedStems,
        "" + message.getTime().getEpochSecond()
    };
  }
}