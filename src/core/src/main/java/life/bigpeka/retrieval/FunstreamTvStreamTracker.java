package life.bigpeka.retrieval;

import com.google.common.collect.Sets;
import life.bigpeka.*;
import org.slf4j.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;
import static java.util.stream.Collectors.toSet;

public class FunstreamTvStreamTracker {
  private final Logger logger = LoggerFactory.getLogger(FunstreamTvStreamTracker.class);

  private final ScheduledExecutorService executor =
      Executors.newSingleThreadScheduledExecutor();

  private final Set<Long> onlineStreams = new HashSet<>();

  private Consumer<String> streamOnlineCallback = s -> {};

  private Consumer<String> streamOfflineCallback = s -> {};

  public void start(long period, TimeUnit timeUnit) {
    executor.scheduleAtFixedRate(() -> {
      try {
        StreamsResponse response = FunstreamTvApi
            .factories
            .create()
            .getStreams(new StreamsRequest("top", "stream", "all"));
        final Set<Long> streamsIds = response.getStreams()
            .stream()
            .map(Stream::getStreamer)
            .map(Streamer::getId)
            .distinct()
            .collect(toSet());

        if (onlineStreams.isEmpty()) {
          onlineStreams.addAll(streamsIds);
          invokeCallback(onlineStreams, streamOnlineCallback);
        } else {
          Set<Long> newStreams = Sets.difference(streamsIds, onlineStreams).immutableCopy();
          Set<Long> wentOffline = Sets.difference(onlineStreams, streamsIds).immutableCopy();
          onlineStreams.removeAll(wentOffline);
          onlineStreams.addAll(newStreams);
          invokeCallback(newStreams, streamOnlineCallback);
          invokeCallback(wentOffline, streamOfflineCallback);
        }
      } catch (Exception e) {
        logger.warn("unexpected exception", e);
      }
    }, 0, period, timeUnit);
  }

  public FunstreamTvStreamTracker onStreamOnline(Consumer<String> streamOnlineCallback) {
    this.streamOnlineCallback = streamOnlineCallback;
    return this;
  }

  public FunstreamTvStreamTracker onStreamOffline(Consumer<String> streamOfflineCallback) {
    this.streamOfflineCallback = streamOfflineCallback;
    return this;
  }

  private void invokeCallback(Set<Long> ids, Consumer<String> callback) {
    ids.stream()
        .map(id -> "stream/" + id)
        .forEach(callback);
  }
}
