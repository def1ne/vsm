package life.bigpeka.retrieval;

import life.bigpeka.*;
import java.util.stream.Collectors;

public class FunstreamTvApiClientExample {
  public static void main(String[] args) {
    //fetch all active streams
    StreamsResponse response = FunstreamTvApi
        .factories
        .create()
        .getStreams(new StreamsRequest("top", "stream", "all"));
    //get only streamers
    System.out.println(response.getStreams()
        .stream()
        .map(Stream::getStreamer)
        .collect(Collectors.toList()));
  }
}
