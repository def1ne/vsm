package life.bigpeka.retrieval;

import java.util.concurrent.TimeUnit;

public class FunstreamTvStreamTrackerExample {
  public static void main(String[] args) {
    new FunstreamTvStreamTracker()
        .onStreamOnline(s -> System.out.println("+" + s))
        .onStreamOffline(s -> System.out.println("-" + s))
        .start(30, TimeUnit.SECONDS);
  }
}
