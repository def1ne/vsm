package life.bigpeka.processing.distributed;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

public class SparkUtils {
  private SparkUtils() {

  }

  public static JavaSparkContext createDefault() {
    return createWithCoreNumber(Runtime.getRuntime().availableProcessors() / 2);
  }

  public static JavaSparkContext createWithCoreNumber(final int coreNumber) {
    return new JavaSparkContext(
        new SparkConf()
            .setAppName("Default")
            .setMaster(String.format("local[%s]", coreNumber)));
  }
}